﻿document.addEventListener('DOMContentLoaded', function() {
  // Event listner for Ok button click.
  var okButton = document.getElementById('btnOk');
  okButton.addEventListener('click', function() {
	  document.getElementById("spanErrorMessage").innerText="";
	submitRequest();
	document.getElementById("spanOutput").innerText="";
	  });
	
	// Event listner for method dropdown change.
	var methodDropdown = document.getElementById('ddlMethod');
  methodDropdown.addEventListener('change', function() {
	document.getElementById("spanOutput").innerText  = "";
	document.getElementById("spanErrorMessage").innerText  = "";
	
	showHideDiv();
	});
	
	// Event listner for url click.
	var restUrl = document.getElementById('restUrl');
	restUrl.addEventListener('click', function() {
   var newURL = "http://localhost:3000/";
  chrome.tabs.create({ url: newURL });
    });
	
	// Event listner for cancel button click.
  var cancelButton = document.getElementById('btnCancel');
  cancelButton.addEventListener('click', function() {
	
    });
	
  }, false);

  // Method to show/hide text boxes depending on method selected.
function showHideDiv() {
    var e = document.getElementById("ddlMethod");
    var selectedMethod = e.options[e.selectedIndex].value;
    
	switch(selectedMethod)
	{
		case "PST":
			document.getElementById('divPostPut').style.display = "block";
			document.getElementById('divGetDelete').style.display = "none";
			break;
		case "PUT":
			document.getElementById('divPostPut').style.display = "block";
			document.getElementById('divGetDelete').style.display = "block";
		break;
		case "GET":
		case "DEL":
			document.getElementById('divPostPut').style.display = "none";
			document.getElementById('divGetDelete').style.display = "block";
		break;
	}
}

// Get/Delete/Update data to jason.db depending on selected method.
function submitRequest() {

        var e = document.getElementById("ddlMethod");
        var selectedMethod = e.options[e.selectedIndex].value;
        var root = 'http://localhost:3000';
		
		if(validate(selectedMethod))
		{
		
        switch(selectedMethod)
        {
            case "GET":
                $.ajax({
                    url: root + '/employee/' + document.getElementById("txtID").value,
                    method: 'GET'
                }).then(function (data) {
					document.getElementById("spanOutput").innerText  = "Employee ID: " + data.id + "\nEmployee Name: " + data.empName + "\nEmployee Designation: " + data.empDesignation;
                    //alert("Employee ID: " + data.id + "\nEmployee Name: " + data.empName + "\nEmployee Designation: " + data.empDesignation);
                });
                break;
            case "PST":
                $.ajax(root + '/employee', {
                    method: 'POST',
                    data: {
                        empName: document.getElementById("txtEmpName").value,
                        empDesignation: document.getElementById("txtEmpDesignation").value
                    }
                }).then(function (data) {
                    //alert("Employee with Name " + document.getElementById("txtEmpName").value + ", saved to DB.");
					document.getElementById("spanOutput").innerText  = "Employee with Name " + document.getElementById("txtEmpName").value + ", saved to DB."
                });
                break;
            case "PUT":
                $.ajax(root + '/employee/' + document.getElementById("txtID").value, {
                    method: 'PUT',
                    data: {
						id: document.getElementById("txtID").value,
                        empName: document.getElementById("txtEmpName").value,
                        empDesignation: document.getElementById("txtEmpDesignation").value
                    }
                }).then(function (data) {
				document.getElementById("spanOutput").innerText  = "Employee with Id " + document.getElementById("txtID").value + ", updated and saved to DB.";
                    //alert("Employee with Id " + document.getElementById("txtID").value + ", updated and saved to DB.");
                });
                break;
            case "DEL":
                $.ajax({
                    url: root + '/employee/' + document.getElementById("txtID").value,
                    method: 'DELETE'
                }).then(function (data) {
					document.getElementById("spanOutput").innerText  = "Employee with Id " + document.getElementById("txtID").value + ", deleted from DB.";
                    //alert("Employee with Id " + document.getElementById("txtID").value + ", deleted from DB.");
                });
                break;
        }
		}
		else
			document.getElementById("spanErrorMessage").innerText="* All fields are required.";
    }
	
	// Check if all values are enterd in textboxes.
function validate(selectedMethod){
	switch(selectedMethod)
	{
		case "PST":
			if(document.getElementById("txtEmpName").value == "" || document.getElementById("txtEmpDesignation").value == "")
				return false;
			else
				return true;
		break;
		case "PUT":
			if(document.getElementById("txtID").value == "" || document.getElementById("txtEmpName").value == "" || document.getElementById("txtEmpDesignation").value == "")
				return false;
			else
				return true;
		break;
		case "GET":
		case "DEL":
			if(document.getElementById("txtID").value == "")
				return false;
			else
				return true;
		break;
		
	}	
}

// Clear values entered in textboxes
function clearControls()
{			
	document.getElementById("divGetDelete").innerText  = "";
	document.getElementById("divPostPut").innerText = "";
}

    
